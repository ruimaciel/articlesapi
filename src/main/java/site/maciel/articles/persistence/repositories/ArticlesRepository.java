package site.maciel.articles.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import site.maciel.articles.domain.Article;

public interface ArticlesRepository extends JpaRepository<Article, Long> {

}
