package site.maciel.articles.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import site.maciel.articles.controllers.contract.resources.ArticleResource;
import site.maciel.articles.domain.Article;
import site.maciel.articles.persistence.repositories.ArticlesRepository;
import site.maciel.articles.controllers.contract.resources.ArticleCollectionResource;

@RestController
@RequestMapping(path = "/articles", produces = "application/hal+json")
public class ArticlesController {

	private ArticlesRepository _articlesRepository;

	@Autowired
	public ArticlesController(ArticlesRepository articlesRepository) {
		_articlesRepository = articlesRepository;
	}

	@GetMapping
	public ResponseEntity<ArticleCollectionResource> GetArticleCollection(@RequestParam(defaultValue = "0") Long offset,
			@RequestParam(defaultValue = "10") Long limit) {

		ArticleCollectionResource resource = new ArticleCollectionResource();

		resource.count = _articlesRepository.count();
		resource.limit = limit;
		resource.offset = offset;

		// add HATEOAS links
		resource.add(linkTo(ArticlesController.class).withSelfRel());
		resource.add(linkTo(HomeController.class).withRel("up"));

		return ResponseEntity.ok().body(resource);
	}

	@GetMapping("/{articleId}")
	public ResponseEntity<ArticleResource> GetArticleById(@PathVariable Long articleId) {

		Optional<Article> entity = _articlesRepository.findById(articleId);

		if (!entity.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		ModelMapper mapper = new ModelMapper();
		ArticleResource resource = mapper.map(entity.get(), ArticleResource.class);

		// add HATEOAS links
		resource.add(linkTo(methodOn(ArticlesController.class).GetArticleById(articleId)).withSelfRel());
		resource.add(linkTo(ArticlesController.class).withRel("up"));

		return ResponseEntity.ok().body(resource);
	}

}
